
export function hitMonster(payload) {
    return { type: "HIT_MONSTER", payload }
};

export function hitBack(payload) {
    return { type: "HIT_BACK", payload }
};
export function healPlayer(payload) {
    return { type: "HEAL_PLAYER", payload }
};
export function regenPlayer(payload) {
    return { type: "REGEN_PLAYER", payload }
};
