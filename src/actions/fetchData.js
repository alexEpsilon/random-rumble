const axios = require('axios')

const asyncData = (data) => {
    return { type: "FETCH_DATA", payload: data };
  };

const fetchData = () => {
    return (dispatch) => {
        axios.get("http://localhost/randomrumbleapi/public/getinitialstate")
        .then(response => {
        //    const state2 = response.data
           dispatch(asyncData(response.data))
        })
        .catch( error => {
            console.log(error);
        })
    }
}

export default fetchData;