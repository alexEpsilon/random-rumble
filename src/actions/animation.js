export function animationDegats(dmg) {
    let animationDesDegatSurMonstre = document.getElementById("monsterCard");
    let affichageDesDegatSurMonstre = document.getElementById("degatSpanMonster");

    animationDesDegatSurMonstre.classList.add('animationDegatsCard');
    affichageDesDegatSurMonstre.classList.add('animationAffichageDesDegats');

    affichageDesDegatSurMonstre.innerHTML = dmg;

    setTimeout(() => {
        affichageDesDegatSurMonstre.classList.remove('animationAffichageDesDegats');
        animationDesDegatSurMonstre.classList.remove('animationDegatsCard');
        affichageDesDegatSurMonstre.innerHTML = "";
    }, 1000)
}

export function animationDegatsJoueur(index, hit) {
    
    //animation des dégats
    let animationDesDegatsSurJoueur = document.getElementById("degatSpanJ" + index);
    let animationDesDegatsSurJoueurShake = document.getElementById("joueur" + index);

    animationDesDegatsSurJoueur.innerHTML = hit;
    animationDesDegatsSurJoueur.classList.add('animationAffichageDesDegats');
    // if (fail) {
    //     animationDesDegatsSurJoueurShake.classList.add('animationDegatsCard');
    // } else {
    //     animationDesDegatsSurJoueur.innerHTML = "FAIL !";
    // }
    setTimeout(() => {
        animationDesDegatsSurJoueur.classList.remove('animationAffichageDesDegats');
        animationDesDegatsSurJoueur.innerHTML = "";
        animationDesDegatsSurJoueurShake.classList.remove('animationDegatsCard');
    }, 700);
}

export function animationHealingJoueur (index){
    let animationHealingSurJoueur = document.getElementById("joueur" + index);

    setTimeout(() => { 
        animationHealingSurJoueur.classList.add("AnimationHeal");
        setTimeout(() => { 
            animationHealingSurJoueur.classList.remove('AnimationHeal');
        }, 300)
    },100)

}

