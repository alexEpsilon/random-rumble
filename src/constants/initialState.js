import { damageCapacities, defenseSkills, manaSkills } from './capacities';

const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
};
export const initialState2 = {
    players: {
        1: {
            name: "John",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 1,
            hasPlayed: false,
            capacities: [
                damageCapacities[getRandomInt(4)],
                damageCapacities[getRandomInt(4)],
                defenseSkills[getRandomInt(4)],
                manaSkills[getRandomInt(4)]
            ]
        },
        2: {
            name: "Jack",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 2,
            hasPlayed: false,
            capacities: [
                damageCapacities[getRandomInt(4)],
                damageCapacities[getRandomInt(4)],
                defenseSkills[getRandomInt(4)],
                manaSkills[getRandomInt(4)]
            ]
        },
        3: {
            name: "Jessy",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 3,
            hasPlayed: false,
            capacities: [
                damageCapacities[getRandomInt(4)], 
                damageCapacities[getRandomInt(4)], 
                defenseSkills[getRandomInt(4)], 
                manaSkills[getRandomInt(4)]
            ]
        },
        4: {
            name: "Jenny",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 4,
            hasPlayed: false,
            capacities: [
                damageCapacities[getRandomInt(4)], 
                damageCapacities[getRandomInt(4)], 
                defenseSkills[getRandomInt(4)], 
                manaSkills[getRandomInt(4)]
            ]
        }
    },
    monster: {
        pv: 800,
        pvMax: 800,
        mana : 1000,
        manaMax : 1000,
        attack: 1
    },
    playerIdWhoPlayed: [],
};
export const initialState = {
    players: {
        1: {
            name: "",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 1,
            hasPlayed: false,
            capacities: []
        },
        2: {
            name: "",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 2,
            hasPlayed: false,
            capacities: []
        },
        3: {
            name: "",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 3,
            hasPlayed: false,
            capacities: []
        },
        4: {
            name: "",
            pv: 100,
            pvMax: 100,
            mana: 30,
            manaMax: 30,
            id: 4,
            hasPlayed: false,
            capacities: [ ]
        }
    },
    monster: {
        pv: 800,
        pvMax: 800,
        mana : 1000,
        manaMax : 1000,
        attack: 1
    },
    playerIdWhoPlayed: [],
};