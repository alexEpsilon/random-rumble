//liste des capacités offensive
export const damageCapacities = [
    {
        type: "offense",
        name: 'Poing',
        description: "",
        heal: 0,
        dmg: 1,
        mana: 1,
        gainMana: 0,
        buff: "",
        combo: 3
    },
    {
        type: "offense",
        name: "Pied",
        description: "",
        heal: 0,
        dmg: 6,
        mana: 5,
        gainMana: 0,
        buff: "",
        combo: 2
    },
    {
        type: "offense",
        name: "Dague",
        description: "",
        heal: 0,
        dmg: 7,
        mana: 10,
        gainMana: 0,
        buff: "",
        combo: 0
    },
    {
        type: "offense",
        name: "Fire",
        description: "",
        heal: 0,
        dmg: 6,
        gainMana: 0,
        mana: 10,
        buff: "",
        combo: 2
    },
];

//liste des capacité deffensive
export const defenseSkills = [
    {
        type: "heal",
        name: 'soin 1',
        description: "",
        heal: 5,
        dmg: 0,
        gainMana: 0,
        mana: 3,
        buff: "",
        combo: 0

    },
    {
        type: "heal",
        name: 'soin 2',
        description: "",
        heal: 7,
        dmg: 0,
        gainMana: 0,
        mana: 5,
        buff: "",
        combo: 0

    },
    {
        type: "heal",
        name: 'soin 3',
        description: "",
        heal: 9,
        dmg: 0,
        gainMana: 0,
        mana: 10,
        buff: "",
        combo: 0

    },
    {
        type: "heal",
        name: 'soin 4',
        description: "",
        heal: 11,
        dmg: 0,
        gainMana: 0,
        mana: 3,
        buff: "",
        combo: 0

    },
];

//liste des capacité régenerante
export const manaSkills = [
    {
        type: "mana",
        name: 'regen 1',
        description: "",
        heal: 0,
        dmg: 0,
        gainMana: 10,
        mana: 0,
        buff: "",
        combo: 0
    },
    {
        type: "mana",
        name: 'regen 2',
        description: "",
        heal: 0,
        dmg: 0,
        gainMana: 15,
        mana: 0,
        buff: "",
        combo: 0
    },
    {
        type: "mana",
        name: 'regen 3',
        description: "",
        heal: -5,
        dmg: 0,
        gainMana: 20,
        mana: 0,
        buff: "",
        combo: 0
    },
    {
        type: "mana",
        name: 'regen 4',
        description: "",
        heal: -10,
        dmg: 0,
        gainMana: 25,
        mana: 0,
        buff: "",
        combo: 0
    },
]