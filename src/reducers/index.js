import { initialState } from '../constants/initialState';
import { animationDegatsJoueur, animationHealingJoueur } from '../actions/animation';

//custom random integer
const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
};

function rootReducer(state = initialState, action ) {
    switch (action.type) {
        case "FETCH_DATA":
            console.log(action.payload)
            state = action.payload
            return state;

        case "RESET_PLAYER":
            console.log(state)
            return {
                ...state,
                playerIdWhoPlayed: []
            }
        
        case "HIT_MONSTER":
            let player = action.payload.player
            return {
                ...state,
                monster: {
                    ...state.monster,
                    pv: state.monster.pv - action.payload.capacity.dmg
                },
                players: {
                    ...state.players,
                    [player.id]: {
                        ...state.players[player.id],
                        hasPlayed: true,
                        mana: state.players[player.id].mana - action.payload.capacity.mana
                    }
                },
                playerIdWhoPlayed: state.playerIdWhoPlayed.concat(player.id)

            }

        case "HIT_BACK": 
            let randomtarget = getRandomInt(4) + 1;
            let hit = state.monster.attack + getRandomInt(15)
            animationDegatsJoueur(randomtarget, hit);

            return {
                ...state,
                players: {
                    ...state.players,
                    [randomtarget]: {
                        ...state.players[randomtarget],
                        pv: state.players[randomtarget].pv - hit,
                    }
                },
                monster: {
                    ...state.monster,
                    mana: state.monster.mana - 100,
                }
            }
        
        case "REGEN_MANA_MONSTER" : 
            return {
                ...state,
                monster: {
                    ...state.monster,
                    mana: state.monster.mana + 100 + getRandomInt(900)
                }
            }
        
        case "HEAL_PLAYER" : 
            animationHealingJoueur(action.target.id);
            return {
                ...state,
                players: {
                    ...state.players,
                    [action.target.id]: {
                        ...state.players[action.target.id],
                        pv: action.target.pv + action.capacity.heal,
                        mana: action.target.mana - action.capacity.mana,
                        hasPlayed: true

                    }
                },
                playerIdWhoPlayed: state.playerIdWhoPlayed.concat(action.target.id)

            }

        case "REGEN_PLAYER" : 
            return {
                ...state,
                players: {
                    ...state.players,
                    [action.target.id]: {
                        ...state.players[action.target.id],
                        mana: action.target.mana + action.capacity.gainMana,
                        hasPlayed: true

                    }
                },
                playerIdWhoPlayed: state.playerIdWhoPlayed.concat(action.target.id)

            }
        

        default:
            return state;;
    }
};
export default rootReducer;