
import rootReducer from "../reducers/index"

const redux = require('redux')
const createStore = redux.createStore
const applyMiddleware = redux.applyMiddleware
const thunk = require('redux-thunk').default




const store = createStore(rootReducer,applyMiddleware(thunk));
export default store;