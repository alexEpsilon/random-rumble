import React from 'react';
import ButtonCapacity from './ButtonCapacity';
import ProgressBar from './ProgressBar';

const PlayerCard = ( props ) => {
    let i = 0
    const displayButton = () => {
        return props.player.capacities.map(capacity => {
            if (props.player.mana <= capacity.mana) {
                return (
                    <ButtonCapacity key={props.player.id + i++} player={props.player} isDisabled={true} capacity={capacity} />
                )
            }
            return (
                <ButtonCapacity key={props.player.id + i++} player={props.player} capacity={capacity} />
            )
        });
    };

    return (
        <div key={props.player.id} className={`col-sm-3 card center ${props.disable}`} id={`joueur${props.player.id}`}>
            <div className="card-body text-center">
                <h5 className="card-title">{props.player.name}</h5>
                <ProgressBar pv={props.player.pv} pvMax={props.player.pvMax} faType='fa-heart' barName=' : pv ' bgType='bg-danger' />
                <ProgressBar pv={props.player.mana} pvMax={props.player.manaMax} faType='fa-fire-alt' barName=' : mana ' />
                <span className="badge badge-danger ml-2 " id={`degatSpanJ${props.player.id}`}></span>
                <div className="row ">
                    {displayButton()}
                </div>
            </div>
        </div>
    )
};

export default PlayerCard;