import React from 'react';
import PlayerCard from './PlayerCard';
import { useDispatch, useSelector } from 'react-redux';

const PlayerList = () => {
  const dispatch = useDispatch()
  const {players, playerIdWhoPlayed} = useSelector((state)=> ({
    players: state.players,
    playerIdWhoPlayed: state.playerIdWhoPlayed,
  }))

  const displayAllPlayers = () => {
    if (playerIdWhoPlayed.length === 4) {
      dispatch({ type: "RESET_PLAYER" })
    }
    return Object.keys(players).map(key => {
      if (!playerIdWhoPlayed.includes(players[key].id)) {
        return (<PlayerCard key={players[key].id} player={players[key]} />)
      } else {
        return (<PlayerCard key={players[key].id} disable='disabled' player={players[key]} />)
      }
    });
  }

  return (
    <div className='row'>
      {displayAllPlayers()}
    </div>
  );
}

export default PlayerList;