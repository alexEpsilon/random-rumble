import React from 'react';
import {  useDispatch,  useSelector } from 'react-redux';
import { animationDegats } from '../actions/animation';
import postState from '../actions/postState';

const ButtonCapacity = (props) => {
    const monster = useSelector(state => state.monster)
    const playerIdWhoPlayed = useSelector(state => state.playerIdWhoPlayed)
    
// all the actions
    const hitMonster = (capacity,player) => dispatch({ type: "HIT_MONSTER", payload: { capacity, player } })
    const hitBack = (target) => dispatch({ type: "HIT_BACK", payload: target })
    const healPlayer = (target, capacity) => dispatch({ type: "HEAL_PLAYER", target, capacity })
    const regenPlayer = (target, capacity) => dispatch({ type: "REGEN_PLAYER", target, capacity })
    const regenManaMonster = () => dispatch({type: "REGEN_MANA_MONSTER"})
    const dispatch = useDispatch()
// all the actions

    const checkDisableds = () => {
        if (props.isDisabled) {
            return 'btn-info disabled';
        } 
        return 'btn-success';
    };

    const monsterAction = () => {
        setTimeout(() => {
            if (monster.mana < 100){
                regenManaMonster()
                return
            }
            hitBack(props.player);
             postState()
        }, 500);
    }

    const playerAction = () => {
        if (playerIdWhoPlayed.includes(props.player.id)) {
            return  
        }
        if (props.capacity.type === "Damages") {
            hitMonster(props.capacity,props.player)
            animationDegats(props.capacity.dmg);
        }
        if (props.capacity.type === "heal")
            healPlayer(props.player, props.capacity)

        if (props.capacity.type === "mana")
            regenPlayer(props.player, props.capacity)

        monsterAction()
    }

    return (
        <button type="button" onClick={playerAction} className={`btn ${checkDisableds()} material-tooltip-main col-12 `}>
            <p> {props.capacity.name} </p>
            <i className="fas fa-bomb"></i> {props.capacity.dmg}
            <i className="fas fa-fire-alt"></i> - {props.capacity.mana}
        </button>
    )
}

export default ButtonCapacity;